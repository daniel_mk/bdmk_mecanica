#include <stdio.h>
#include <locale.h>

#include "bdmk/bdmk_load.h"
#include "src/funcoes_uteis.h"
#include "src/struct.h"
#include "src/geral.h"
#include "src/cliente.h"
#include "src/ordem.h"

int main_menu()
{
    int op;
    cls();
    print_c('_', 40);
    printf("\n|%-38s|", " BDMK MEC�NICA");
    printf("\n|"); print_c('-', 38); printf("|");
    printf("\n|%-38s|", " 1.Clientes");
    printf("\n|%-38s|", " 2.Ordens de servi�o");
    printf("\n|%-38s|\n", " 0.Sair");
    printf("|"); print_c('-', 38); printf("|");
    printf("\n\nOp��o > ");
    scanf("%d", &op); getchar();
    return op;
}

int main()
{
    if(win()) setlocale(LC_ALL, "Portuguese");
    int op1, op2;
    Opcoes_Lista op_cliente, op_ordem;

    load_opcoes(&op_cliente, 1);
    load_opcoes(&op_ordem, 2);

    while(op1 = main_menu()){
        if(op1 == 1){
            op2 = -1;
            do{
                switch(op2){
                case 1:
                    cadastrar_cliente();
                    break;
                case 2:
                    ver_cliente();
                    break;
                case 3:
                    voltar_pag(&op_cliente);
                    break;
                case 4:
                    prox_pag(&op_cliente);
                    break;
                case 5:
                    pesquisa_cliente(&op_cliente);
                    break;
                case 6:
                    config_cliente(&op_cliente);
                    break;
                }
                clientes_lista(&op_cliente);
            }while(op2 = clientes_menu());
        }
        if(op1 == 2){
            op2 = -1;
            do{
                switch(op2){
                case 1:
                    cadastrar_ordem();
                    break;
                case 2:
                    ver_ordem();
                    break;
                case 3:
                    voltar_pag(&op_ordem);
                    break;
                case 4:
                    prox_pag(&op_ordem);
                    break;
                case 5:
                    pesquisa_ordem(&op_ordem);
                    break;
                case 6:
                    config_ordem(&op_ordem);
                    break;
                }
                ordem_lista(&op_ordem);
            }while(op2 = ordem_menu());
        }
    }
    return 0;
}
