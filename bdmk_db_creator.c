#include <stdio.h>
#include "bdmk/bdmk_load.h"

int main2()
{
    Database * db = &bdmk_struct;
    char filename[32];
    int i, j, len;

    printf("File name: ");
    scanf("%s", filename);
    printf("Database name (useless): ");
    scanf("%s", (*db).desc);
    printf("Number of tables: ");
    scanf("%d", &(*db).ntables);
    for(i=0; i<(*db).ntables; i++){
        printf("%d - Table name: ", i+1);
        scanf(" %s", (*db).tables[i].desc);
        printf("%d - Number of fields: ", i+1);
        scanf("%d", &(*db).tables[i].nfields);
        printf("%d - Maximum number of rows: ", i+1);
        scanf("%d", &(*db).tables[i].max_nrows);
        (*db).tables[i].nrows = 0;
        for(j=0; j<(*db).tables[i].nfields; j++){
            printf("%d - Field name: ", j+1);
            scanf(" %s", (*db).tables[i].fields[j].desc);
            printf("%d - Field length (0 for int): ", j+1);
            scanf("%d", &len);
            (*db).tables[i].fields[j].len = !len ? -10 : len;
        }
    }
    bdmk_create(filename);
    return 0;
}

