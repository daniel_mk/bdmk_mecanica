#ifndef CLIENTE
#define CLIENTE

#include "struct.h"

void voltar_pag(Opcoes_Lista * op);
void prox_pag(Opcoes_Lista * op);
void load_opcoes(Opcoes_Lista * op, int id);
void save_opcoes(Opcoes_Lista * op, int id);

#endif
