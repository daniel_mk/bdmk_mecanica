#ifndef STRUCT
#define STRUCT

typedef struct{
    int pag, res_por_pag, max_pag;
    char campo_pesquisa[32], pesquisa[50];
    char campo_ordenar[32], tipo_ordenar[10];
} Opcoes_Lista;

typedef struct{
    int dia, mes, ano;
} Data;

#endif
