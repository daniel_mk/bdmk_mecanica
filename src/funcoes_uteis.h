#ifndef FUNCOES_UTEIS
#define FUNCOES_UTEIS

#include "struct.h"

void cls();
void print_c(char c, int n);
void my_fgets(char * str, int len);
int get_date();
Data format_date(int data);
void press_msg(char * msg);
void abrir_caixa();
void fechar_caixa();

#endif
