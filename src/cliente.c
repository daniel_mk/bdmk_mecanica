#include "cliente.h"
#include "funcoes_uteis.h"
#include "../bdmk/bdmk_load.h"

void clientes_lista(Opcoes_Lista * op)
{
    Query * q;
    char select_query[256];
    sprintf(select_query, "TABLE Cliente; WHERE %s ~ %s ORDER BY %s %s OFFSET %s",
            (*op).campo_pesquisa, "%s", (*op).campo_ordenar, (*op).tipo_ordenar, "%d");
    bdmk_open("BDMK_Mecanica");
    q = bdmk_select(select_query, (*op).pesquisa, ((*op).pag - 1)*(*op).res_por_pag);

    char resultados[100];
    if(!(*op).pesquisa[0]){
        sprintf(resultados, " Todos os resultados (%d)", (*q).totalrows);
    } else {
        sprintf(resultados, " Pesquisa por %s: '%s' (%d)", (*op).campo_pesquisa, (*op).pesquisa, (*q).totalrows);
    }

    char pag[10];
    int max_pag = (*q).totalrows/(*op).res_por_pag + !((*q).totalrows%(*op).res_por_pag == 0);
    (*op).max_pag = !max_pag ? ++max_pag : max_pag;
    sprintf(pag, " p.%d/%d", (*op).pag, max_pag);
    int i, id, divida;
    char * nome, * telefone;

    cls();
    abrir_caixa();
    printf("\n|%-68s|", " CLIENTES");
    printf("\n|%-54s|%-13s|\n", resultados, pag);
    printf("|%-06s|%-29s|%-17s|%-13s|\n", " Id", " Nome", " Telefone", " D�vida");
    fechar_caixa();
    if(!(*q).error){
        if(!(*q).nrows) printf("|%-68s|\n", " Nenhum resultado encontrado.");
        for(; (*q).row<(*op).res_por_pag && (*q).row<(*q).nrows; (*q).row++){
            id = bdmk_fetch_int(q, "id");
            nome = bdmk_fetch_str(q, "nome");
            telefone = bdmk_fetch_str(q, "telefone1");
            divida = bdmk_fetch_int(q, "divida");
            printf("| %04d | %-28s| %-16s| R$ %5d.00 |\n", id, nome, telefone, divida);
            free(nome); free(telefone);
        }
        for(i=(*q).row; i<(*op).res_por_pag; i++){
            printf("|%-68s|\n", "");
        }

    } else {
        printf("Vishhhhh\n");
    }
    free(q);
    fechar_caixa();
    printf("\n");
    bdmk_close();
}

int clientes_menu()
{
    int op;
    abrir_caixa();
    printf("\n|%-34s|%-16s|%-16s|", " 1.Cadastrar cliente", " 3.Voltar p�g.", " 4.Pr�xima p�g.");
    printf("\n|%-34s|%-33s|", " 2.Ver cliente", " 5.Pesquisa");
    printf("\n|%-68s|", " 6.Configurar exibi��o de resultados");
    printf("\n|%-68s|\n", " 0.Menu principal");
    fechar_caixa();
    printf("\nOp��o > ");
    scanf("%d", &op); getchar();
    return op;
}

void cadastrar_cliente()
{
    char nome[32], cpf[20], telefone1[20], telefone2[20];
    int i, id, divida, data;

    nome[0] = cpf[0] = telefone1[0] = telefone2[0] = 0;

    for(i=0; i<5; i++){
        cls();
        abrir_caixa();
        printf("\n|%-68s|\n", " CADASTRAR CLIENTE");
        fechar_caixa();
        printf("|%-25s| %-41s|", " Nome", nome);
        printf("\n|%-25s| %-41s|", " CPF", cpf);
        printf("\n|%-25s| %-41s|", " Telefone principal", telefone1);
        printf("\n|%-25s| %-41s|\n", " Telefone secund�rio", telefone2);
        fechar_caixa();
        switch(i){
        case 0:
            printf("\nNome > ");
            my_fgets(nome, 32);
            break;
        case 1:
            printf("\nCPF > ");
            my_fgets(cpf, 20);
            break;
        case 2:
            printf("\nTelefone principal > ");
            my_fgets(telefone1, 20);
            break;
        case 3:
            printf("\nTelefone secund�rio > ");
            my_fgets(telefone2, 20);
            break;
        case 4:
            press_msg("Enter para continuar.");
        }
    }

    Query * q;

    bdmk_open("BDMK_Mecanica");
    q = bdmk_select("TABLE Cliente; ORDER BY id DESC");

    if((*q).nrows == 0) id = 1;
    else id = bdmk_fetch_int(q, "id") + 1;
    divida = 0;
    data = get_date();
    free(q);

    char insert_query[] = "TABLE Cliente; id, nome, cpf, telefone1, telefone2, divida, data";

    bdmk_insert(insert_query, id, nome, cpf, telefone1, telefone2, divida, data);
    bdmk_close();
}

void ver_cliente()
{
    int id, divida, data_cadastro, op, cod, valor, pago, data;
    char * nome, * cpf, * telefone1, * telefone2, msg[100];
    Query * q; Data d;

    printf("Identificador do cliente: ");
    scanf("%d", &id); getchar();

    bdmk_open("BDMK_Mecanica");
    q = bdmk_select("TABLE Cliente; WHERE id = %d", id);

    cls();
    if((*q).nrows == 0){
        free(q); bdmk_close();
        sprintf(msg, "Nenhum usu�rio com o id %d. Enter para voltar.", id);
        press_msg(msg);
    } else {
        nome = bdmk_fetch_str(q, "nome");
        cpf = bdmk_fetch_str(q, "cpf");
        telefone1 = bdmk_fetch_str(q, "telefone1");
        telefone2 = bdmk_fetch_str(q, "telefone2");
        divida = bdmk_fetch_int(q, "divida");
        data_cadastro = bdmk_fetch_int(q, "data");

        d = format_date(data_cadastro);

        abrir_caixa();
        printf("\n|%-68s|\n", " VER CLIENTE");
        fechar_caixa();
        sprintf(msg, "%d", id);
        printf("|%-25s| %-41s|", " Id", msg);
        printf("\n|%-25s| %-41s|", " Nome", nome);
        printf("\n|%-25s| %-41s|", " CPF", cpf);
        printf("\n|%-25s| %-41s|", " Telefone principal", telefone1);
        printf("\n|%-25s| %-41s|", " Telefone secund�rio", telefone2);
        sprintf(msg, "R$ %d.00", divida);
        printf("\n|%-25s| %-41s|", " D�vida", msg);
        sprintf(msg, "%02d/%02d/%d", d.dia, d.mes, d.ano);
        printf("\n|%-25s| %-41s|\n", " Data de cadastro", msg);
        fechar_caixa();

        free(nome); free(cpf);
        free(telefone1); free(telefone2);

        free(q);
        q = bdmk_select("TABLE Ordem_Servico; WHERE cliente_id = %d", id);
        if((*q).nrows){
            printf("|%-68s|\n", " ORDENS DE SERVI�O");
            printf("|%-06s|%-33s|%-13s|%-13s|", " Cod", " Data de cria��o", " Valor", " Pago");
        }
        for(; (*q).row < (*q).nrows; (*q).row++){
            cod = bdmk_fetch_int(q, "cod");
            valor = bdmk_fetch_int(q, "valor");
            pago = bdmk_fetch_int(q, "pago");
            data = bdmk_fetch_int(q, "data");
            d = format_date(data);
            sprintf(msg, "%02d/%02d/%d", d.dia, d.mes, d.ano);
            printf("\n| %04d | %-32s| R$ %5d.00 |%-12s |", cod, msg, valor, pago ? " Sim" : " N�o");
        }
        if((*q).nrows){
            printf("\n");
            fechar_caixa();
        }
        free(q); bdmk_close();

        printf("\n");
        abrir_caixa();
        printf("\n|%-22s|%-22s|%-22s|\n", " 1.Editar", " 2.Excluir", " 0.Voltar");
        fechar_caixa();
        printf("\nOp��o > ");
        scanf("%d", &op); getchar();
        if(op == 1){
            editar_cliente(id);
        } else if(op == 2){
            deletar_cliente(id);
            press_msg("Cadastro deletado. Enter para voltar.");
        }
    }
}

void pesquisa_cliente(Opcoes_Lista * op)
{
    int i; char pesquisa[100];
    cls();
    abrir_caixa();
    printf("\n|%-68s|", " Pesquisar por:");
    printf("\n|%-68s|", " 1.Nome");
    printf("\n|%-68s|", " 2.CPF");
    printf("\n|%-68s|\n", " 0.Limpar pesquisa");
    fechar_caixa();
    printf("\nOp��o: ");
    scanf("%d", &i); getchar();
    switch(i){
    case 0:
        strcpy((*op).pesquisa, "");
        break;
    case 1:
        printf("Pesquisar por nome: ");
        my_fgets(pesquisa, 100);
        (*op).pag = 1;
        strcpy((*op).campo_pesquisa, "nome");
        strcpy((*op).pesquisa, pesquisa);
        break;
    case 2:
        printf("Pesquisar por CPF: ");
        my_fgets(pesquisa, 100);
        (*op).pag = 1;
        strcpy((*op).campo_pesquisa, "cpf");
        strcpy((*op).pesquisa, pesquisa);
    }
}

void deletar_cliente(int id)
{
    bdmk_open("BDMK_Mecanica");
    bdmk_delete("TABLE Cliente; WHERE id = %d", id);
    bdmk_close();
}

void editar_cliente(int id)
{
    Query * q;
    int op;
    char aux[100];
    char * nome, * cpf, * telefone1, * telefone2;

    do{
        bdmk_open("BDMK_Mecanica");
        q = bdmk_select("TABLE Cliente; WHERE id = %d", id);

        nome = bdmk_fetch_str(q, "nome");
        cpf = bdmk_fetch_str(q, "cpf");
        telefone1 = bdmk_fetch_str(q, "telefone1");
        telefone2 = bdmk_fetch_str(q, "telefone2");

        cls();
        abrir_caixa();
        printf("\n|%-68s|\n", " EDITAR CLIENTE");
        fechar_caixa();
        sprintf(aux, "%d", id);
        printf("|%-25s| %-41s|", " Id", aux);
        printf("\n|%-25s| %-41s|", " 1.Nome", nome);
        printf("\n|%-25s| %-41s|", " 2.CPF", cpf);
        printf("\n|%-25s| %-41s|", " 3.Telefone principal", telefone1);
        printf("\n|%-25s| %-41s|", " 4.Telefone secund�rio", telefone2);
        printf("\n|%-68s|\n", " 0.Sair");
        fechar_caixa();

        free(nome); free(cpf);
        free(telefone1); free(telefone2);
        free(q); bdmk_close();

        printf("\nOp��o > ");
        scanf("%d", &op); getchar();
        switch(op){
        case 1:
            printf("\nNome > ");
            my_fgets(aux, 32);
            bdmk_open("BDMK_Mecanica");
            bdmk_update("TABLE Cliente; nome WHERE id = %d", aux, id);
            bdmk_close();
            break;
        case 2:
            printf("\nCPF > ");
            my_fgets(aux, 20);
            bdmk_open("BDMK_Mecanica");
            bdmk_update("TABLE Cliente; cpf WHERE id = %d", aux, id);
            bdmk_close();
            break;
        case 3:
            printf("\nTelefone principal > ");
            my_fgets(aux, 20);
            bdmk_open("BDMK_Mecanica");
            bdmk_update("TABLE Cliente; telefone1 WHERE id = %d", aux, id);
            bdmk_close();
            break;
        case 4:
            printf("\nTelefone secund�rio > ");
            my_fgets(aux, 20);
            bdmk_open("BDMK_Mecanica");
            bdmk_update("TABLE Cliente; telefone2 WHERE id = %d", aux, id);
            bdmk_close();
            break;
        }
    }while(op);
}

void config_cliente(Opcoes_Lista * op)
{
    int i, j;
    cls();
    abrir_caixa();
    printf("\n|%-68s|", " Configurar exibi��o de resultados para clientes:");
    printf("\n|%-68s|", " 1.Ordena��o");
    printf("\n|%-68s|", " 2.Quantidade de resultados por p�gina");
    printf("\n|%-68s|\n", " 0.Voltar");
    fechar_caixa();
    printf("\nOp��o: ");
    scanf("%d", &i); getchar();
    switch(i){
    case 1:
        cls();
        abrir_caixa();
        printf("\n|%-68s|", " Ordenar clientes por:");
        printf("\n|%-68s|", " 1.Nome (Ascendente)");
        printf("\n|%-68s|", " 2.D�vida (Descendente)");
        printf("\n|%-68s|", " 3.Identifica��o (Ascendente)");
        printf("\n|%-68s|\n", " 4.Identifica��o (Descendente)");
        fechar_caixa();
        printf("\nOp��o: ");
        scanf("%d", &j); getchar();
        switch(j){
        case 1:
            strcpy((*op).campo_ordenar, "nome");
            strcpy((*op).tipo_ordenar, "ASC");
            break;
        case 2:
            strcpy((*op).campo_ordenar, "divida");
            strcpy((*op).tipo_ordenar, "DESC");
            break;
        case 3:
            strcpy((*op).campo_ordenar, "id");
            strcpy((*op).tipo_ordenar, "ASC");
            break;
        case 4:
            strcpy((*op).campo_ordenar, "id");
            strcpy((*op).tipo_ordenar, "DESC");
        }
        break;
    case 2:
        printf("Quantidade de resultados por p�gina [5,30]: ");
        scanf("%d", &j); getchar();
        if(j < 5) j = 5;
        if(j > 30) j = 30;
        (*op).res_por_pag = j;
        (*op).pag = 1;
        break;
    }
    save_opcoes(op, 1);
}
