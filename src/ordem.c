#include "ordem.h"
#include "funcoes_uteis.h"
#include "../bdmk/bdmk_load.h"

void ordem_lista(Opcoes_Lista * op)
{
    Query * q1, * q2;
    char select_query[256];
    sprintf(select_query, "TABLE Ordem_Servico; WHERE %s ~ %s ORDER BY %s %s OFFSET %s",
            (*op).campo_pesquisa, "%s", (*op).campo_ordenar, (*op).tipo_ordenar, "%d");
    bdmk_open("BDMK_Mecanica");
    q1 = bdmk_select(select_query, (*op).pesquisa, ((*op).pag - 1)*(*op).res_por_pag);
    char resultados[100];
    if(!(*op).pesquisa[0]){
        sprintf(resultados, " Todos os resultados (%d)", (*q1).totalrows);
    } else {
        sprintf(resultados, " Pesquisa por %s: '%s' (%d)", (*op).campo_pesquisa, (*op).pesquisa, (*q1).totalrows);
    }

    char pag[10];
    int max_pag = (*q1).totalrows/(*op).res_por_pag + !((*q1).totalrows%(*op).res_por_pag == 0);
    (*op).max_pag = !max_pag ? ++max_pag : max_pag;
    sprintf(pag, " p.%d/%d", (*op).pag, max_pag);
    int i, cliente_id, cod, valor, pago;
    char * cliente;

    cls();
    abrir_caixa();
    printf("\n|%-68s|", " ORDENS DE SERVI�O");
    printf("\n|%-54s|%-13s|\n", resultados, pag);
    printf("|%-06s|%-33s|%-13s|%-13s|\n", " Cod", " Cliente", " Valor", " Pago");
    fechar_caixa();
    if(!(*q1).error){
        if(!(*q1).nrows) printf("|%-68s|\n", " Nenhum resultado encontrado.");
        for(; (*q1).row<(*op).res_por_pag && (*q1).row<(*q1).nrows; (*q1).row++){
            cod = bdmk_fetch_int(q1, "cod");
            cliente_id = bdmk_fetch_int(q1, "cliente_id");
            q2 = bdmk_select("TABLE Cliente; WHERE id = %d", cliente_id);
            if(!(*q2).nrows){
                cliente = malloc(30);
                strcpy(cliente, "*Cliente n�o encontrado*");
            } else {
                cliente = bdmk_fetch_str(q2, "nome");
            }
            free(q2);
            valor = bdmk_fetch_int(q1, "valor");
            pago = bdmk_fetch_int(q1, "pago");
            printf("| %04d | %-32s| R$ %5d.00 |%-12s |\n", cod, cliente, valor, pago ? " Sim" : " N�o");
            free(cliente);
        }
        for(i=(*q1).row; i<(*op).res_por_pag; i++){
            printf("|%-68s|\n", "");
        }

    } else {
        printf("Vishhhhh\n");
    }
    free(q1);
    fechar_caixa();
    printf("\n");
    bdmk_close();
}

int ordem_menu()
{
    int op;
    abrir_caixa();
    printf("\n|%-34s|%-16s|%-16s|", " 1.Nova ordem de servi�o", " 3.Voltar p�g.", " 4.Pr�xima p�g.");
    printf("\n|%-34s|%-33s|", " 2.Ver ordem de servi�o", " 5.Pesquisa");
    printf("\n|%-68s|", " 6.Configurar exibi��o de resultados");
    printf("\n|%-68s|\n", " 0.Menu principal");
    fechar_caixa();
    printf("\nOp��o > ");
    scanf("%d", &op); getchar();
    return op;
}

int selecionar_cliente(Opcoes_Lista * op_cliente)
{
    int id_cliente = 0, op;
    do{
        clientes_lista(op_cliente);
        abrir_caixa();
        printf("\n|%-34s|%-16s|%-16s|", " 1.Selecionar cliente", " 3.Voltar p�g.", " 4.Pr�xima p�g.");
        printf("\n|%-34s|%-33s|\n", "", " 5.Pesquisa");
        fechar_caixa();
        printf("\nOp��o > ");
        scanf("%d", &op); getchar();
        switch(op){
        case 1:
            printf("Identificador do cliente: ");
            scanf("%d", &id_cliente); getchar();
            break;
        case 3:
            voltar_pag(op_cliente);
            break;
        case 4:
            prox_pag(op_cliente);
            break;
        case 5:
            pesquisa_cliente(op_cliente);
        }
    } while(!id_cliente);
    return id_cliente;
}

void mostrar_servico(Query ** q)
{
    char * desc;
    int valor, len, pos = 0;

    desc = bdmk_fetch_str(*q, "descricao");
    valor = bdmk_fetch_int(*q, "valor");

    len = strlen(desc);
    printf("| %-47.47s |       R$ %4d.00 |\n", &desc[pos], valor);
    pos += 47;
    while(len > pos){
        printf("| %-47.47s |%-18s|\n", &desc[pos], "");
        pos += 47;
    }
}

void cadastrar_ordem()
{
    int i, cliente_id, op, valor, valor_antigo, total, data_ordem, cod, id;
    char cliente[32], * cli, placa[20], desc[256];
    Query * q;
    char insert_ordem[] = "TABLE Ordem_Servico; cod, cliente_id, placa, data, valor, pago";
    char insert_servico[] = "TABLE Servico; id, ordem_cod, descricao, valor";

    cliente[0] = placa[0] = total = cod = 0;

    Opcoes_Lista op_cliente;

    op_cliente.pag = 1;
    op_cliente.res_por_pag = 6;
    strcpy(op_cliente.campo_pesquisa, "nome");
    strcpy(op_cliente.pesquisa, "");
    strcpy(op_cliente.campo_ordenar, "nome");
    strcpy(op_cliente.tipo_ordenar, "ASC");

    for(i=0; i<4; i++){
        cls();
        abrir_caixa();
        printf("\n|%-68s|\n", " CADASTRAR ORDEM DE SERVI�O");
        fechar_caixa();
        printf("|%-25s| %-41s|", " Cliente", cliente);
        printf("\n|%-25s| %-41s|\n", " Placa do ve�culo", placa);
        fechar_caixa();
        printf("|%-68s|", " Servi�os", placa);
        printf("\n|%-49s| %-17s|\n", " Descri��o", "Valor");
        fechar_caixa();

        if(cod){
            bdmk_open("BDMK_Mecanica");
            q = bdmk_select("TABLE Servico; WHERE ordem_cod = %d", cod);
            for(; (*q).row < (*q).nrows; (*q).row++){
                mostrar_servico(&q);
            }
            free(q);
            q = bdmk_select("TABLE Ordem_Servico; WHERE cod = %d", cod);
            total = bdmk_fetch_int(q, "valor");
            free(q);
            bdmk_close();
        } else {
            total = 0;
        }

        printf("|%-49s| Total R$ %4d.00 |\n", "", total);
        fechar_caixa();
        switch(i){
        case 0:
            press_msg("\nEnter para selecionar cliente.");
            cliente_id = selecionar_cliente(&op_cliente);
            bdmk_open("BDMK_Mecanica");
            q = bdmk_select("TABLE Cliente; WHERE id = %d", cliente_id);
            if((*q).error || !(*q).nrows){
                i--;
            } else {
                cli = bdmk_fetch_str(q, "nome");
                strcpy(cliente, cli);
                free(cli);
            }
            free(q);
            bdmk_close();
            break;
        case 1:
            printf("\nPlaca do ve�culo > ");
            my_fgets(placa, 20);

            bdmk_open("BDMK_Mecanica");
            q = bdmk_select("TABLE Ordem_Servico; ORDER BY cod DESC");
            if((*q).nrows == 0) cod = 1;
            else cod = bdmk_fetch_int(q, "cod") + 1;
            free(q);
            data_ordem = get_date();
            bdmk_insert(insert_ordem, cod, cliente_id, placa, data_ordem, 0, 0);
            bdmk_close();
            break;
        case 2:
            printf("\nDescri��o de servi�o > ");
            my_fgets(desc, 256);
            printf("Valor > ");
            scanf("%d", &valor); getchar();

            bdmk_open("BDMK_Mecanica");
            q = bdmk_select("TABLE Servico; ORDER BY id DESC");
            if((*q).nrows == 0) id = 1;
            else id = bdmk_fetch_int(q, "id") + 1;
            free(q);
            bdmk_insert(insert_servico, id, cod, desc, valor);

            q = bdmk_select("TABLE Ordem_Servico; WHERE cod = %d", cod);
            valor_antigo = bdmk_fetch_int(q, "valor");
            free(q);
            bdmk_update("TABLE Ordem_Servico; valor WHERE cod = %d", valor_antigo + valor, cod);

            bdmk_close();
            break;
        case 3:
            abrir_caixa();
            printf("\n|%-34s|%-33s|\n", " 1.Adicionar servi�o", " 0.Concluir ordem");
            fechar_caixa();
            printf("\nOp��o > ");
            scanf("%d", &op); getchar();
            if(op){
                i -= 2;
            } else {
                mudar_divida(cliente_id, total);
            }
        }
    }
    press_msg("Ordem conclu�da. Enter para voltar.");
}

void ver_ordem()
{
    int cod, cliente_id, data_cadastro, valor, pago, op, total, nrows;
    char * cliente, * placa, msg[100];
    Query * q1, * q2; Data d;

    printf("C�digo da ordem de servi�o: ");
    scanf("%d", &cod); getchar();

    bdmk_open("BDMK_Mecanica");
    q1 = bdmk_select("TABLE Ordem_Servico; WHERE cod = %d", cod);
    nrows = (*q1).nrows;
    free(q1);
    bdmk_close();

    if(!nrows){
        cls();
        sprintf(msg, "Nenhuma ordem de servi�o com o c�digo %d. Enter para voltar.", cod);
        press_msg(msg);
    } else {
        do{
            cls();
            bdmk_open("BDMK_Mecanica");
            q1 = bdmk_select("TABLE Ordem_Servico; WHERE cod = %d", cod);

            placa = bdmk_fetch_str(q1, "placa");
            cliente_id = bdmk_fetch_int(q1, "cliente_id");
            data_cadastro = bdmk_fetch_int(q1, "data");
            valor = bdmk_fetch_int(q1, "valor");
            pago = bdmk_fetch_int(q1, "pago");
            free(q1);
            d = format_date(data_cadastro);

            q2 = bdmk_select("TABLE Cliente; WHERE id = %d", cliente_id);
            if(!(*q2).nrows){
                cliente = malloc(30);
                strcpy(cliente, "*Cliente n�o encontrado*");
            } else {
                cliente = bdmk_fetch_str(q2, "nome");
            }
            free(q2);

            abrir_caixa();
            printf("\n|%-68s|\n", " VER ORDEM DE SERVI�O");
            fechar_caixa();
            sprintf(msg, "%d", cod);
            printf("|%-25s| %-41s|", " C�digo", msg);
            printf("\n|%-25s| %-41s|", " Cliente", cliente);
            printf("\n|%-25s| %-41s|", " Placa do ve�culo", placa);
            sprintf(msg, "%02d/%02d/%d", d.dia, d.mes, d.ano);
            printf("\n|%-25s| %-41s|", " Data de cria��o", msg);
            sprintf(msg, "R$ %d.00", valor);
            printf("\n|%-25s| %-41s|", " Valor", msg);
            printf("\n|%-25s| %-41s|\n", " Pago", pago ? "Sim" : "N�o");
            fechar_caixa();

            free(cliente); free(placa);

            q2 = bdmk_select("TABLE Servico; WHERE ordem_cod = %d", cod);
            for(; (*q2).row < (*q2).nrows; (*q2).row++){
                mostrar_servico(&q2);
            }
            free(q2);

            q2 = bdmk_select("TABLE Ordem_Servico; WHERE cod = %d", cod);
            total = bdmk_fetch_int(q2, "valor");
            free(q2);
            bdmk_close();
            printf("|%-49s| Total R$ %4d.00 |\n", "", total);
            fechar_caixa();

            printf("\n");
            abrir_caixa();
            printf("\n|%-22s|%-22s|%-22s|\n", " 1.Editar", " 2.Excluir", " 0.Voltar");
            printf("|%-68s|\n", pago ? " 3.Marcar como n�o pago" : " 3.Marcar como pago");
            fechar_caixa();
            printf("\nOp��o > ");
            scanf("%d", &op); getchar();
            switch(op){
            case 1:
                cls();
                press_msg("�, n�o cheguei nessa parte.");
                break;
            case 2:
                deletar_ordem(cod);
                if(!pago) mudar_divida(cliente_id, -1*valor);
                press_msg("Ordem deletada. Enter para voltar.");
                break;
            case 3:
                bdmk_open("BDMK_Mecanica");
                bdmk_update("TABLE Ordem_Servico; pago WHERE cod = %d", !pago, cod);
                bdmk_close();
                mudar_divida(cliente_id, valor*(pago ? 1 : -1));
                break;
            }
        }while(op && op != 2);
    }
}

void pesquisa_ordem(Opcoes_Lista * op)
{
    int i; char pesquisa[100];
    cls();
    abrir_caixa();
    printf("\n|%-68s|", " Pesquisar por:");
    printf("\n|%-68s|", " 1.Placa");
    printf("\n|%-68s|\n", " 0.Limpar pesquisa");
    fechar_caixa();
    printf("\nOp��o: ");
    scanf("%d", &i); getchar();
    switch(i){
    case 0:
        strcpy((*op).pesquisa, "");
        break;
    case 1:
        printf("Pesquisar por placa: ");
        my_fgets(pesquisa, 100);
        (*op).pag = 1;
        strcpy((*op).campo_pesquisa, "placa");
        strcpy((*op).pesquisa, pesquisa);
    }
}

void deletar_ordem(int cod)
{
    bdmk_open("BDMK_Mecanica");
    bdmk_delete("TABLE Ordem_Servico; WHERE cod = %d", cod);
    bdmk_delete("TABLE Servico; WHERE ordem_cod = %d", cod);
    bdmk_close();
}

void mudar_divida(int cliente_id, int valor)
{
    Query * q; int valor_antigo;
    bdmk_open("BDMK_Mecanica");
    q = bdmk_select("TABLE Cliente; WHERE id = %d", cliente_id);
    valor_antigo = bdmk_fetch_int(q, "divida");
    free(q);
    bdmk_update("TABLE Cliente; divida WHERE id = %d", valor_antigo + valor, cliente_id);
    bdmk_close();
}

void editar_ordem(int id)
{
    // Nada aqui
}

void config_ordem(Opcoes_Lista * op)
{
    int i, j;
    cls();
    abrir_caixa();
    printf("\n|%-68s|", " Configurar exibi��o de resultados para ordens se servi�o:");
    printf("\n|%-68s|", " 1.Ordena��o");
    printf("\n|%-68s|", " 2.Quantidade de resultados por p�gina");
    printf("\n|%-68s|\n", " 0.Voltar");
    fechar_caixa();
    printf("\nOp��o: ");
    scanf("%d", &i); getchar();
    switch(i){
    case 1:
        cls();
        abrir_caixa();
        printf("\n|%-68s|", " Ordenar ordens de servi�o por:");
        printf("\n|%-68s|", " 1.C�digo (Descendente)");
        printf("\n|%-68s|", " 2.C�digo (Ascendente)");
        printf("\n|%-68s|\n", " 3.Valor (Descendente)");
        fechar_caixa();
        printf("\nOp��o: ");
        scanf("%d", &j); getchar();
        switch(j){
        case 1:
            strcpy((*op).campo_ordenar, "cod");
            strcpy((*op).tipo_ordenar, "DESC");
            break;
        case 2:
            strcpy((*op).campo_ordenar, "cod");
            strcpy((*op).tipo_ordenar, "ASC");
            break;
        case 3:
            strcpy((*op).campo_ordenar, "valor");
            strcpy((*op).tipo_ordenar, "DESC");
        }
        break;
    case 2:
        printf("Quantidade de resultados por p�gina [5,30]: ");
        scanf("%d", &j); getchar();
        if(j < 5) j = 5;
        if(j > 30) j = 30;
        (*op).res_por_pag = j;
        (*op).pag = 1;
        break;
    }
    save_opcoes(op, 2);
}
