#ifndef CLIENTE
#define CLIENTE

#include "struct.h"

void clientes_lista(Opcoes_Lista * op);
int clientes_menu();
void cadastrar_cliente();
void ver_cliente();
void pesquisa_cliente(Opcoes_Lista * op);
void deletar_cliente(int id);
void editar_cliente(int id);
void config_cliente(Opcoes_Lista * op);

#endif
