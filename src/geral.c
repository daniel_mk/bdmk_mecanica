#include "geral.h"
#include "struct.h"
#include "../bdmk/bdmk_struct.h"

void voltar_pag(Opcoes_Lista * op)
{
    if((*op).pag > 1) (*op).pag--;
}

void prox_pag(Opcoes_Lista * op)
{
    if((*op).pag < (*op).max_pag) (*op).pag++;
}

void load_opcoes(Opcoes_Lista * op, int id)
{
    Query * q; char * aux;

    (*op).pag = 1;
    if(id == 1){
        strcpy((*op).campo_pesquisa, "nome");
        strcpy((*op).pesquisa, "");
    } else {
        strcpy((*op).campo_pesquisa, "placa");
        strcpy((*op).pesquisa, "");
    }

    bdmk_open("BDMK_Mecanica");
    q = bdmk_select("TABLE Opcoes; WHERE id = %d", id);
    if(!(*q).nrows){
        char query_insert[] = "TABLE Opcoes; id, res_por_pag, campo_ordenar, tipo_ordenar";
        if(id == 1){
            (*op).res_por_pag = 8;
            strcpy((*op).campo_ordenar, "nome");
            strcpy((*op).tipo_ordenar, "ASC");
            bdmk_insert(query_insert, id, 8, "nome", "ASC");
        } else {
            (*op).res_por_pag = 8;
            strcpy((*op).campo_ordenar, "cod");
            strcpy((*op).tipo_ordenar, "DESC");
            bdmk_insert(query_insert, id, 8, "cod", "DESC");
        }
        free(q); bdmk_close();
    } else {
        (*op).res_por_pag = bdmk_fetch_int(q, "res_por_pag");
        aux = bdmk_fetch_str(q, "campo_ordenar");
        strcpy((*op).campo_ordenar, aux); free(aux);
        aux = bdmk_fetch_str(q, "tipo_ordenar");
        strcpy((*op).tipo_ordenar, aux); free(aux);
        free(q); bdmk_close();
    }
}

void save_opcoes(Opcoes_Lista * op, int id)
{
    char query_update[] = "TABLE Opcoes; res_por_pag, campo_ordenar, tipo_ordenar WHERE id = %d";
    bdmk_open("BDMK_Mecanica");
    bdmk_update(query_update, (*op).res_por_pag, (*op).campo_ordenar, (*op).tipo_ordenar, id);
    bdmk_close();
}
