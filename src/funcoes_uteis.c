#include "funcoes_uteis.h"
#include "struct.h"
#include <stdio.h>
#include <time.h>

#ifdef linux
#define WIN 0
#endif
#ifdef _WIN32
#define WIN 1
#endif

void cls()
{
    if(WIN) system("cls"); else system("clear");
}

void print_c(char c, int n)
{
    while(n--) printf("%c", c);
}

void my_fgets(char * str, int len)
{
    fgets(str, len, stdin);
    str[strlen(str)-1] = 0;
}

int get_date()
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int ano, mes, dia;

    ano = tm.tm_year + 1900;
    mes = tm.tm_mon + 1;
    dia = tm.tm_mday;

    return ano*10000 + mes*100 + dia;
}

Data format_date(int data)
{
    Data d;

    d.dia = data%100; data /= 100;
    d.mes = data%100; data /= 100;
    d.ano = data;

    return d;
}

void press_msg(char * msg)
{
    printf("\n%s", msg);
    getchar();
}

void abrir_caixa()
{
    print_c('_', 70);
}

void fechar_caixa()
{
    printf("|"); print_c('-', 68); printf("|\n");
}

int win()
{
    return WIN;
}

