#ifndef ORDEM
#define ORDEM

#include "../bdmk/bdmk_struct.h"
#include "struct.h"

void ordem_lista(Opcoes_Lista * op);
int ordem_menu();
int selecionar_cliente(Opcoes_Lista * op);
void mostrar_servico(Query ** q);
void cadastrar_ordem();
void ver_ordem();
void pesquisa_ordem(Opcoes_Lista * op);
void deletar_ordem(int cod);
void mudar_divida(int cliente_id, int valor);
void editar_ordem(int id);
void config_ordem(Opcoes_Lista * op);

#endif
