#ifndef BDMK_CRUD
#define BDMK_CRUD
#include <stdio.h>
#include "bdmk_struct.h"

extern FILE * bdmk_file;

void bdmk_open(char * file_name);

int bdmk_insert(char * table_name, ...);
Query * bdmk_select(char * query, ...);
int bdmk_fetch_int(Query * q, char * field_name);
char * bdmk_fetch_str(Query * q, char * field_name);
int bdmk_update(char * query, ...);
int bdmk_delete(char * query, ...);

#endif
