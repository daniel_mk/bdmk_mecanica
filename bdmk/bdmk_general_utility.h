#ifndef BDMK_GENERAL_UTILITY
#define BDMK_GENERAL_UTILITY

int _bdmk_cin_array(char c, char * arr);
int _bdmk_cin_range(char c, char s, char e);
int _bdmk_is_word(char c, int pos);

char * _bdmk_get_word(int pos, int * final_pos, char * str, int type);

int min(int a, int b);
int max(int a, int b);
void _bdmk_lcase(char * str);

#endif
