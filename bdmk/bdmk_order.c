#include "bdmk_struct.h"
#include "bdmk_order.h"

int _bdmk_compare_quick(int tbl_i, int * v, int * fields, int n, char type, int * int_val, char ** str_val)
{
    Table * t = &bdmk_struct.tables[tbl_i];
    int i = 0, val, field_index;
    char * row, * str;
    _bdmk_set_pos(v[n]);
    row = _bdmk_read_row((*t).row_len);

    while(fields[i] != -1){
        field_index = fields[i];
        if((*t).fields[field_index].len == -10){
            val = _bdmk_get_int_row(tbl_i, row, field_index);
            if(type == '<'){
                if(val < int_val[field_index]){
                    free(row); return 1;
                } else if(val > int_val[field_index]){
                    free(row); return 0;
                }
            } else {
                if(val > int_val[field_index]){
                    free(row); return 1;
                } else if(val < int_val[field_index]){
                    free(row); return 0;
                }
            }
        } else {
            str = _bdmk_get_str_row(tbl_i, row, field_index);
            _bdmk_lcase(str);
            if(type == '<'){
                if(strcmp(str, str_val[field_index]) < 0){
                    free(str); free(row); return 1;
                } else if(strcmp(str, str_val[field_index]) > 0){
                    free(str); free(row); return 0;
                }
            } else {
                if(strcmp(str, str_val[field_index]) > 0){
                    free(str); free(row); return 1;
                } else if(strcmp(str, str_val[field_index]) < 0){
                    free(str); free(row); return 0;
                }
            }
            free(str);
        }
        i++;
    }
    free(row);
    return 0;
}

int _bdmk_in_range_order(int start, int end, int r_start, int r_end)
{
    if(r_start >= start && r_start <= end) return 1;
    if((r_end >= start && r_end <= end)) return 1;
    if(start > r_start && start < r_end) return 1;
    return 0;
}

void _bdmk_quick(int tbl_i, int * v, int start, int end, int r_start, int r_end, int * fields, int asc)
{
    int i, j, pivo, aux, count;
    int int_val[32], field_index;
    char * row, * str_val[32];
    Table * t = &bdmk_struct.tables[tbl_i];

    i = start; j = end;
    pivo = (i+j)/2; count = 0;
    _bdmk_set_pos(v[pivo]);
    row = _bdmk_read_row((*t).row_len);
    while(fields[count] != -1){
        field_index = fields[count++];
        if((*t).fields[field_index].len == -10){
            int_val[field_index] = _bdmk_get_int_row(tbl_i, row, field_index);
        } else {
            str_val[field_index] = malloc((*t).fields[field_index].len + 1);
            str_val[field_index] = _bdmk_get_str_row(tbl_i, row, field_index);
            _bdmk_lcase(str_val[field_index]);
        }
    }
    free(row);

    while(i<j){
        if(asc){
            while(_bdmk_compare_quick(tbl_i, v, fields, i, '<', int_val, &str_val)) i++;
            while(_bdmk_compare_quick(tbl_i, v, fields, j, '>', int_val, &str_val)) j--;
        } else {
            while(_bdmk_compare_quick(tbl_i, v, fields, i, '>', int_val, &str_val)) i++;
            while(_bdmk_compare_quick(tbl_i, v, fields, j, '<', int_val, &str_val)) j--;
        }
        if(i<=j){
            aux = v[i];
            v[i] = v[j];
            v[j] = aux;
            i++; j--;
        }
    }
    count = 0;
    while(fields[count] != -1){
        field_index = fields[count++];
        if((*t).fields[field_index].len != -10) free(str_val[field_index]);
    }
    if(j > start && _bdmk_in_range_order(start, j, r_start, r_end)){
        _bdmk_quick(tbl_i, v, start, j, r_start, r_end, fields, asc);
    }
    if(i < end && _bdmk_in_range_order(i, end, r_start, r_end)){
        _bdmk_quick(tbl_i, v, i, end, r_start, r_end, fields, asc);
    }
}

int _bdmk_order(int table_index, char * query, int * rows, int offset, int desc)
{
    char * aux;
    int ncond, pos, fields[16]; memset(fields, -1, sizeof(fields));
    Table * t = &bdmk_struct.tables[table_index];
    ncond = pos = 0;
    do{
        aux = _bdmk_get_word(pos, &pos, query, 0), aux[0];
        if((fields[ncond++] = _bdmk_get_field_index(table_index, aux)) == -1){
            printf("O campo %s nao existe na tabela %s.\n", aux, (*t).desc);
            free(aux);
            return 0;
        }
        free(aux);
    }while(aux = _bdmk_get_word(pos, &pos, query, 1), !strcmp(aux, ","));
    free(aux);

    int num_rows = 0;
    while(rows[num_rows] != -1) num_rows++;
    _bdmk_quick(table_index, rows, 0, num_rows-1, offset, offset+50, fields, !desc);
    return 1;
}

