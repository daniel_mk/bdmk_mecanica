#include "bdmk_struct.h"
#include "bdmk_file_management.h"

void _bdmk_set_pos(int pos)
{
    fseek(bdmk_file, pos, SEEK_SET);
}

int _bdmk_get_pos()
{
    return ftell(bdmk_file);
}

int _bdmk_write_str(char * str, int len)
{
    if(strlen(str) > len) return 0;
    char * new_str = malloc(len + 1);

    memset(new_str, ' ', len);
    strcpy(new_str, str);
    new_str[strlen(str)] = 3;
    new_str[len] = 0;

    fputs(new_str, bdmk_file); free(new_str);
    return 1;
}

int _bdmk_write_int(int num)
{
    if(num > 999999999 || num < -999999999) return 0;

    char str[10];
    sprintf(str, "%d", num);
    _bdmk_write_str(str, 10);

    return 1;
}

char * _bdmk_read(int len)
{
    char * str = malloc(len + 1); int i;
    fread(str, len, 1, bdmk_file);
    for(i=0; i<len; i++){
        if(str[i] == 3){
            str[i] = 0;
            break;
        }
    }
    return str;
}

int _bdmk_read_int()
{
    char * num = _bdmk_read(10);
    int n = atoi(num); free(num);
    return n;
}

char * _bdmk_read_row(int len)
{
    char * str = malloc(len + 1);
    fread(str, len, 1, bdmk_file);
    return str;
}
