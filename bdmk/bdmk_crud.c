#include <stdarg.h>
#include "bdmk_crud.h"

void bdmk_open(char * file_name)
{
    char full_file_name[36], *aux;
    int i, j, row_len, tab = 42;

    strcpy(full_file_name, file_name);
    strcat(full_file_name, ".txt");

    bdmk_file = fopen(full_file_name, "r+b");
    Database * db = &bdmk_struct;

    strcpy((*db).desc, (aux = _bdmk_read(32))); free(aux);
    (*db).ntables = _bdmk_read_int();
    for(i=0; i<(*db).ntables; i++){
        strcpy((*db).tables[i].desc, (aux = _bdmk_read(32))); free(aux);
        (*db).tables[i].max_nrows = _bdmk_read_int();
        (*db).tables[i].nrows = _bdmk_read_int();
        (*db).tables[i].nfields = _bdmk_read_int();
        for(row_len=j=0; j<(*db).tables[i].nfields; j++){
            strcpy((*db).tables[i].fields[j].desc, (aux = _bdmk_read(32))); free(aux);
            (*db).tables[i].fields[j].len = _bdmk_read_int();
            row_len += abs((*db).tables[i].fields[j].len);
        }
        tab = _bdmk_set_table_info(i, tab);
        _bdmk_set_pos(_bdmk_get_pos() + row_len * (*db).tables[i].max_nrows);
    }
}

void bdmk_close()
{
    fclose(bdmk_file);
}

int bdmk_insert(char * query, ...)
{
    int table_index, field_index, field_len, file_pos;
    int int_values[32], i;
    char * string_values[32], ** fields;
    va_list valist;

    if(fields = _bdmk_insert_filter(query), !fields[0][0]){
        printf("Sintaxe incorreta para insercao de dados.\n");
        return 0;
    }

    if((table_index = _bdmk_get_table_index(fields[0])) == -1){
        printf("A tabela %s nao foi encontrada nesse banco de dados.\n", fields[0]);
        return 0;
    }

    Table * t = &bdmk_struct.tables[table_index];

    va_start(valist, (*t).nfields);
    for(i=1; i<=(*t).nfields; i++){
        field_index = _bdmk_get_field_index(table_index, fields[i]);
        if(field_index == -1){
            printf("Campo %s inexistente na tabela %s.\n", fields[i], fields[0]);
            return 0;
        }
        free(fields[i]);
        field_len = (*t).fields[field_index].len;
        if(field_len == -10){
            int_values[field_index] = va_arg(valist, int);
        } else {
            string_values[field_index] = (char*) malloc(field_len + 1);
            strcpy(string_values[field_index], va_arg(valist, char *));
        }
    }
    va_end(valist); free(fields);

    if((file_pos = _bdmk_get_first_e_row(table_index)) == -1){
        printf("Tabela %s cheia.\n", (*t).desc);
        return 0;
    }

    _bdmk_set_pos(file_pos);
    for(i=0; i<(*t).nfields; i++){
        field_len = (*t).fields[i].len;
        if(field_len == -10){
            _bdmk_write_int(int_values[i]);
        } else {
            _bdmk_write_str(string_values[i], field_len);
            free(string_values[i]);
        }
    }
    _bdmk_change_nrows(table_index, 1);
    return 1;
}

Query * bdmk_select(char * query, ...)
{
    int table_index, * result;
    char ** fields;
    Query * q = malloc(sizeof(Query)); (*q).error = 1;
    (*q).row = (*q).nrows = 0;

    if(fields = _bdmk_select_filter(query), !fields[0][0]){
        printf("Sintaxe incorreta para selecao de dados.\n");
        return q;
    }

    if((table_index = _bdmk_get_table_index(fields[0])) == -1){
        printf("A tabela %s nao existe.\n", fields[0]);
        return q;
    }
    (*q).table_index = table_index;

    va_list valist;
    Table * t = &bdmk_struct.tables[table_index];

    va_start(valist, 99);
    result = _bdmk_where(table_index, fields[0], fields[1], &valist);

    if(result[0] == -1){
        free(result);
        (*q).totalrows = 0;
        (*q).error = 0;
        return q;
    }

    int cont, offset, desc = 0;

    cont = offset = 0;
    if(fields[3][0]) offset = va_arg(valist, int);
    va_end(valist);

    if(fields[2][0]){
        if(strstr(query, "DESC")) desc = 1;
        if(!_bdmk_order(table_index, fields[2], result, offset, desc)) return q;
    }

    while(result[cont] != -1 && (*q).nrows < 50){
        if(cont >= offset){
            (*q).row_desc[(*q).nrows] = result[cont];
            (*q).nrows++;
        }
        cont++;
    }
    while(result[cont] != -1) cont++;
    (*q).totalrows = cont;
    free(result);

    (*q).error = 0;
    return q;
}

int bdmk_fetch_int(Query * q, char * field_name)
{
    char * error = malloc(1); error[0] = 0;
    int field_index, ret;
    if((field_index = _bdmk_get_field_index((*q).table_index, field_name)) == -1){
        printf("O campo %s nao existe nessa pesquisa.\n");
        return error;
    }

    Table * t = &bdmk_struct.tables[(*q).table_index];
    Field * f = &bdmk_struct.tables[(*q).table_index].fields[field_index];

    char * row;
    _bdmk_set_pos((*q).row_desc[(*q).row]);
    row = _bdmk_read_row((*t).row_len);

    ret = _bdmk_get_int_row((*q).table_index, row, field_index); free(row);
    return ret;
}

char * bdmk_fetch_str(Query * q, char * field_name)
{
    char * error = malloc(1); error[0] = 0;
    int field_index;
    if((field_index = _bdmk_get_field_index((*q).table_index, field_name)) == -1){
        printf("O campo %s nao existe nessa pesquisa.\n");
        return error;
    }

    Table * t = &bdmk_struct.tables[(*q).table_index];

    char * row, * word;
    _bdmk_set_pos((*q).row_desc[(*q).row]);
    row = _bdmk_read_row((*t).row_len);
    word = _bdmk_get_str_row((*q).table_index, row, field_index);
    free(row);

    return word;
}

int bdmk_update(char * query, ...)
{
    int table_index, * result, i, j;
    char ** fields;

    if(fields = _bdmk_update_filter(query), !fields[0][0]){
        printf("Sintaxe incorreta para atualizacao de dados.\n");
        return -1;
    }

    if((table_index = _bdmk_get_table_index(fields[0])) == -1){
        printf("A tabela %s nao existe.\n", fields[0]);
        return -1;
    }

    Table * t = &bdmk_struct.tables[table_index];
    int field_index, field_len, file_pos, int_values[32];
    char * string_values[32];
    va_list valist;

    int nparams = 0;
    while(fields[++nparams][0]);

    int menos = 1;
    if(!strstr(query, "WHERE")) menos = 0;

    va_start(valist, 99);
    for(i=1; i<nparams-menos; i++){
        field_index = _bdmk_get_field_index(table_index, fields[i]);
        if(field_index == -1){
            printf("Campo %s inexistente na tabela %s.\n", fields[i], fields[0]);
            return 0;
        }
        field_len = (*t).fields[field_index].len;
        if(field_len == -10){
            int_values[field_index] = va_arg(valist, int);
        } else {
            string_values[field_index] = (char*) malloc(field_len + 1);
            strcpy(string_values[field_index], va_arg(valist, char *));
        }
    }

    result = _bdmk_where(table_index, fields[0], fields[nparams-menos], &valist);
    va_end(valist);

    for(i=0; result[i] != -1; i++){
        for(j=1; j<nparams-menos; j++){
            field_index = _bdmk_get_field_index(table_index, fields[j]);
            _bdmk_set_pos(result[i] + (*t).fields[field_index].pos);
            field_len = (*t).fields[field_index].len;
            if(field_len == -10){
                _bdmk_write_int(int_values[field_index]);
            } else {
                _bdmk_write_str(string_values[field_index], field_len);
            }
        }
    }
    free(fields); free(result);

    return i;
}

int bdmk_delete(char * query, ...)
{
    int table_index, * result, i;
    char ** fields;

    if(fields = _bdmk_delete_filter(query), !fields[0][0]){
        printf("Sintaxe incorreta para delecao de dados.\n");
        return -1;
    }

    if((table_index = _bdmk_get_table_index(fields[0])) == -1){
        printf("A tabela %s nao existe.\n", fields[0]);
        return -1;
    }

    va_list valist;
    Table * t = &bdmk_struct.tables[table_index];

    va_start(valist, 99);
    result = _bdmk_where(table_index, fields[0], fields[1], &valist);
    va_end(valist);

    for(i=0; result[i] != -1; i++){
        _bdmk_set_pos(result[i]);
        _bdmk_write_str("", (*t).row_len);
    }
    free(result);

    _bdmk_change_nrows(table_index, -1*i);
    return i;
}
