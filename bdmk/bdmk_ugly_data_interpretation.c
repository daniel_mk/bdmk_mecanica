#include "bdmk_ugly_data_interpretation.h"
#include "bdmk_struct.h"
#include <stdarg.h>

int _bdmk_get_int_row(int table_index, char * row, int index)
{
    int ret;
    Table * t = &bdmk_struct.tables[table_index];
    char * word = malloc(abs((*t).fields[index].len) + 1);
    sprintf(word, "%.*s", abs((*t).fields[index].len), &row[(*t).fields[index].pos]);
    ret = atoi(word); free(word);
    return ret;
}

int _bdmk_cmp_int(int ti, char * row, int * values, int a, int op, int b)
{
    int val1, val2;
    val1 = a < 0 ? values[abs(a)-1] : _bdmk_get_int_row(ti, row, a);
    val2 = b < 0 ? values[abs(b)-1] : _bdmk_get_int_row(ti, row, b);
    switch(op){
        case '=':
            return val1 == val2;
        case '<':
            return val1 < val2;
        case '>':
            return val1 > val2;
        case '<' + '=':
            return val1 <= val2;
        case '>' + '=':
            return val1 >= val2;
        case '>' + '<':
            return val1 != val2;
    }
    return 0;
}

char * _bdmk_get_str_row(int table_index, char * row, int index)
{
    Table * t = &bdmk_struct.tables[table_index];
    char * word = malloc((*t).fields[index].len + 1); int i;

    sprintf(word, "%.*s", (*t).fields[index].len, &row[(*t).fields[index].pos]);
    for(i=0; i<(*t).fields[index].len; i++){
        if(word[i] == 3){
            word[i] = 0;
            break;
        }
    }
    return word;
}

int _bdmk_cmp_str(int ti, char * row, char ** values, int a, int op, int b)
{
    char * val1, * val2;
    char str1[100], str2[100];

    val1 = a < 0 ? &(*(values[abs(a)-1])) : _bdmk_get_str_row(ti, row, a);
    val2 = b < 0 ? &(*(values[abs(b)-1])) : _bdmk_get_str_row(ti, row, b);

    switch(op){
        case '=':
            return !strcmp(val1, val2);
        case '<':
            return strcmp(val1, val2) < 0;
        case '>':
            return strcmp(val1, val2) > 0;
        case '<' + '=':
            return strcmp(val1, val2) <= 0;
        case '>' + '=':
            return strcmp(val1, val2) >= 0;
        case '>' + '<':
            return strcmp(val1, val2) != 0;
        case '~':
            strcpy(str1, val1); _bdmk_lcase(str1);
            strcpy(str2, val2); _bdmk_lcase(str2);
            return strstr(str1, str2) != 0;
    }
    return 0;
}

int * _bdmk_where(int table_index, char * table_name, char * query, va_list * valist)
{
    Query_Map * map;
    char * error;
    int * error_int = malloc(sizeof(int)); error_int[0] = -1;

    if(query[0]){
        if(map = _bdmk_make_query_map(table_index, query, &error), (*map).err_no){
            switch((*map).err_no){
                case -9999:
                    printf("O parametro %s nao eh valido.\n", error);
                break;
                case -9998:
                    printf("O campo %s nao existe na tabela %s.\n", error, table_name);
                break;
                case -9997:
                    printf("Operador invalido: %s.\n", error);
                break;
                case -9996:
                    printf("As comparacoes devem ter ao menos um campo.\n");
                break;
                case -9995:
                    printf("O parametro para o campo %s nao corresponde a seu tipo.\n", error);
                break;
            }
            return error_int;
        }
    } else {
        map = malloc(sizeof(Query_Map)); (*map).nparam = (*map).ncmp = 0;
    }

    int i, j, int_values[16];
    char * row, * str_values[16];

    for(i=0; i<(*map).nparam; i++){
        switch((*map).param_type[i]){
            case 's':
                str_values[i] = va_arg(*valist, char*);
            break;
            case 'd':
                int_values[i] = va_arg(*valist, int);
            break;
            case 'c':
                int_values[i] = (int) va_arg(*valist, char);
        }
    }

    Table * t = &bdmk_struct.tables[table_index];
    int is_valid, found_rows, valid_rows, index_a, index_b, op, field_len;
    int * result = malloc(((*t).nrows + 1) * sizeof(int));

    _bdmk_set_pos((*t).data_pos);
    found_rows = valid_rows = 0; memset(result, -1, ((*t).nrows + 1) * sizeof(int));
    for(i=0; i < (*t).max_nrows && found_rows != (*t).nrows; i++){
        row = _bdmk_read_row((*t).row_len);
        is_valid = 1;
        if(!_bdmk_is_row_empty(row, (*t).row_len)){
            found_rows++;
            for(j=0; j<(*map).ncmp; j++){
                index_a = (*map).cmp_desc[j][0];
                op = (*map).cmp_desc[j][1];
                index_b = (*map).cmp_desc[j][2];
                field_len = (*t).fields[max(index_a, index_b)].len;
                if(!j || (*map).cmp_desc[j-1][3] == '&'){
                    if(field_len == -10 || field_len == 1){
                        is_valid *= _bdmk_cmp_int(table_index, row, &int_values, index_a, op, index_b);
                    } else {
                        is_valid *= _bdmk_cmp_str(table_index, row, &str_values, index_a, op, index_b);
                    }
                } else {
                    if(field_len == -10 || field_len == 1){
                        is_valid += _bdmk_cmp_int(table_index, row, &int_values, index_a, op, index_b);
                    } else {
                        is_valid += _bdmk_cmp_str(table_index, row, &str_values, index_a, op, index_b);
                    }
                }
            }
            if(is_valid){
                result[valid_rows++] = _bdmk_get_pos() - (*t).row_len;
            }
        }
        free(row);
    }
    free(map);
    return result;
}
