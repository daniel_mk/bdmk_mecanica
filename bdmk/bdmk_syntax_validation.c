#include "bdmk_struct.h"
#include "bdmk_syntax_validation.h"

char * _bdmk_get_table_filter(int pos, int * final_pos, char * query, char * last_str)
{
    char * error = malloc(1); error[0] = 0;
    char * table_name, * clear1, * clear2;
    if(!strcmp((clear1 = _bdmk_get_word(pos, &pos, query, 0)), "TABLE")
            && strcmp((table_name = _bdmk_get_word(pos, &pos, query, 0)), "")
            && !strcmp((clear2 = _bdmk_get_word(pos, &pos, query, 1)), last_str)){
        *final_pos = pos;
        free(clear1); free(clear2);
        return table_name;
    }
    free(clear1); free(clear2); free(table_name);
    return error;
}

char ** _bdmk_insert_filter(char * query)
{
    int pos = 0, i = 0, len;
    char ** data = malloc(32*sizeof(char*));
    char ** error = malloc(sizeof(char*)); error[0] = malloc(1); error[0][0] = 0;
    char * aux = malloc(1); aux[0] = ',';

    if(data[i++] = _bdmk_get_table_filter(pos, &pos, query, ";"), data[0][0]){
        len = strlen(query);
        if(!strcmp((data[i++] = _bdmk_get_word(pos, &pos, query, 0)), ""))
            return error;
        while(pos < len && aux[0]){
			free(aux);
            aux = _bdmk_get_word(pos, &pos, query, 1);
            if(!strcmp(aux, "")) continue;
            if(strcmp(aux, ",") || !strcmp((data[i++] = _bdmk_get_word(pos, &pos, query, 0)), "")){
                free(aux); return error;
            }
        }
		free(aux);
        aux = _bdmk_get_word(pos, &pos, query, 3); free(aux);
        if(pos < len) return error;
        return data;
    }
    return error;
}

char ** _bdmk_select_filter(char * query)
{
    int pos = 0, len;
    char ** data = malloc(4*sizeof(char*));
    char ** error = malloc(sizeof(char*)); error[0] = malloc(1); error[0][0] = 0;
    char * aux, * stmt, * order;

    len = strlen(query); data[0] = data[1] = data[2] = data[3] = *error;
    if(data[0] = _bdmk_get_table_filter(0, &pos, query, ""), data[0][0]){
        if(pos == len)
            return data;
    } else if(data[0] = _bdmk_get_table_filter(0, &pos, query, ";"), data[0][0]){
        aux = _bdmk_get_word(pos, &pos, query, 0);
        if(!aux[0])
            return error;
        else if(!strcmp(aux, "WHERE")){
            stmt = malloc(len); strcpy(stmt, "");
            do{
                if(aux = _bdmk_get_word(pos, &pos, query, 2), !aux[0]){
                    free(aux); return error;
                }
                strcat(stmt, aux); free(aux);
                if(aux = _bdmk_get_word(pos, &pos, query, 1), !aux[0]){
                    free(aux); return error;
                }
                strcat(stmt, aux); free(aux);
                if(aux = _bdmk_get_word(pos, &pos, query, 2), !aux[0]){
                    free(aux); return error;
                }
                strcat(stmt, aux);
                if(pos < len){
                    if(aux = _bdmk_get_word(pos, &pos, query, 1), strlen(aux) > 1 ||
                            (aux[0] && !_bdmk_cin_array(aux[0], "&|"))){
                        free(aux); return error;
                    }
                    strcat(stmt, aux);
                }
            }while(!strcmp(aux, "&") || !strcmp(aux, "|"));
			free(aux);
            data[1] = stmt;
            aux = _bdmk_get_word(pos, &pos, query, 0);
        }
        if(!strcmp(aux, "ORDER")){
            order = malloc(len); strcpy(order, "");
            if(aux = _bdmk_get_word(pos, &pos, query, 0), strcmp(aux, "BY")){
                free(aux); return error;
            }
            if(aux = _bdmk_get_word(pos, &pos, query, 0), !aux[0]){
                free(aux); return error;
            }
            strcat(order, aux);
            while(pos < len && aux[0]){
				free(aux);
                aux = _bdmk_get_word(pos, &pos, query, 1);
                strcat(order, aux);
                if(!strcmp(aux, "")) continue;
                if(strcmp(aux, ",") || !strcmp((aux = _bdmk_get_word(pos, &pos, query, 0)), "")){
                    free(aux); return error;
                }
                strcat(order, aux);
            }
			free(aux);
            data[2] = order;
            aux = _bdmk_get_word(pos, &pos, query, 0);
            if(!strcmp(aux, "ASC") || !strcmp(aux, "DESC")){
                free(aux);
                aux = _bdmk_get_word(pos, &pos, query, 0);
            }
        }
        if(!strcmp(aux, "OFFSET")){
            if(aux = _bdmk_get_word(pos, &pos, query, 2), strcmp(aux, "%d")){
                free(aux); return error;
            }
            free(aux); aux = _bdmk_get_word(pos, &pos, query, 0);
            data[3] = malloc(1);
            data[3][0] = 1;
        }
        if(strcmp(aux, "")){
            free(aux); return error;
        }
        free(aux);
        aux = _bdmk_get_word(pos, &pos, query, 3); free(aux);
        if(pos < len) return error;
        return data;
    }
    return error;
}

char ** _bdmk_delete_filter(char * query)
{
    int pos = 0, len;
    char ** data = malloc(2*sizeof(char*));
    char ** error = malloc(sizeof(char*)); error[0] = malloc(1); error[0][0] = 0;
    char * aux, * stmt;

    len = strlen(query); data[0] = data[1] = *error;
    if(data[0] = _bdmk_get_table_filter(0, &pos, query, ""), data[0][0]){
        if(pos == len)
            return data;
    } else if(data[0] = _bdmk_get_table_filter(0, &pos, query, ";"), data[0][0]){
        aux = _bdmk_get_word(pos, &pos, query, 0);
        if(!aux[0]){
			free(aux);
			return error;
		}
        else if(!strcmp(aux, "WHERE")){
            stmt = malloc(len); strcpy(stmt, "");
            do{
                if(aux = _bdmk_get_word(pos, &pos, query, 2), !aux[0]){
                    free(aux); return error;
                }
                strcat(stmt, aux); free(aux);
                if(aux = _bdmk_get_word(pos, &pos, query, 1), !aux[0]){
                    free(aux); return error;
                }
                strcat(stmt, aux); free(aux);
                if(aux = _bdmk_get_word(pos, &pos, query, 2), !aux[0]){
                    free(aux); return error;
                }
                strcat(stmt, aux);
                if(pos < len){
                    if(aux = _bdmk_get_word(pos, &pos, query, 1), strlen(aux) > 1 ||
                            (aux[0] && !_bdmk_cin_array(aux[0], "&|"))){
                        free(aux); return error;
                    }
                    strcat(stmt, aux);
                }
            }while(!strcmp(aux, "&") || !strcmp(aux, "|"));
			free(aux);
            data[1] = stmt;
        }
        aux = _bdmk_get_word(pos, &pos, query, 3); free(aux);
        if(pos < len) return error;
        return data;
    }
    return error;
}

char ** _bdmk_update_filter(char * query)
{
    int pos = 0, i = 0, len;
    char ** data = malloc(33*sizeof(char*));
    char ** error = malloc(sizeof(char*)); error[0] = malloc(1); error[0][0] = 0;
    char * aux, * stmt;
    aux = malloc(1); aux[0] = ',';

    if(data[i++] = _bdmk_get_table_filter(pos, &pos, query, ";"), data[0][0]){
        len = strlen(query);
        if(!strcmp((data[i++] = _bdmk_get_word(pos, &pos, query, 0)), ""))
            return error;
        while(pos < len && aux[0]){
			free(aux);
            aux = _bdmk_get_word(pos, &pos, query, 1);
            if(!strcmp(aux, "")) continue;
            if(strcmp(aux, ",") || !strcmp((data[i++] = _bdmk_get_word(pos, &pos, query, 0)), "")){
                free(aux); return error;
            }
        }
		free(aux);
        if(pos < len){
            if(aux = _bdmk_get_word(pos, &pos, query, 0), strcmp(aux, "WHERE")){
                free(aux); return error;
            }
        } else {
            data[i] = *error;
            return data;
        }
    }

    stmt = malloc(len); strcpy(stmt, "");
    do{
        if(aux = _bdmk_get_word(pos, &pos, query, 2), !aux[0]){
            free(aux); return error;
        }
        strcat(stmt, aux); free(aux);
        if(aux = _bdmk_get_word(pos, &pos, query, 1), !aux[0]){
            free(aux); return error;
        }
        strcat(stmt, aux); free(aux);
        if(aux = _bdmk_get_word(pos, &pos, query, 2), !aux[0]){
            free(aux); return error;
        }
        strcat(stmt, aux);
        if(pos < len){
            if(aux = _bdmk_get_word(pos, &pos, query, 1), strlen(aux) > 1 ||
                    (aux[0] && !_bdmk_cin_array(aux[0], "&|"))){
                free(aux); return error;
            }
            strcat(stmt, aux);
        }
    }while(!strcmp(aux, "&") || !strcmp(aux, "|"));
	free(aux);
    data[i++] = stmt;

    aux = _bdmk_get_word(pos, &pos, query, 3); free(aux);
    if(pos < len) return error;
    data[i] = *error;
    return data;
}

