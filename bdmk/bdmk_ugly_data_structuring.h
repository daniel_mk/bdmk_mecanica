#ifndef BDMK_UGLY_DATA_STRUCTURING
#define BDMK_UGLY_DATA_STRUCTURING
#include "bdmk_struct.h"

int _bdmk_param_type(char * word);
int _bdmk_sub_make_query_map(int * pos, char * query, int index, int table_index, Query_Map ** map, char ** error);
Query_Map * _bdmk_make_query_map(int table_index, char * query, char ** error);

#endif
