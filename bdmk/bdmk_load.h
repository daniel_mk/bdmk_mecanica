#ifndef BDMK_LOAD
#define BDMK_LOAD

#include "bdmk_struct.h"
#include "bdmk_file_management.h"
#include "bdmk_general_utility.h"
#include "bdmk_system_utility.h"
#include "bdmk_syntax_validation.h"
#include "bdmk_ugly_data_structuring.h"
#include "bdmk_ugly_data_interpretation.h"
#include "bdmk_database_creation.h"
#include "bdmk_order.h"
#include "bdmk_crud.h"
#include <stdio.h>

FILE * bdmk_file;

#endif
